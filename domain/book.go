package domain

import "context"

type Book struct {
	ID     int    `json:"-"`
	Title  string `json:"title"`
	Author string `json:"author"`
}

type BookUsecase interface {
	AmbilBuku(ctx context.Context, ID int) Book
}

type BookRepo interface {
	GetBook(ctx context.Context, ID int) []Book
}
