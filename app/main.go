package main

import (
	httpDelivery "bookapp/book/delivery/http"
	bRepo "bookapp/book/repository"
	bUsecase "bookapp/book/usecase"
	"time"

	"github.com/gin-gonic/gin"
)

func main() {
	gin := gin.Default()
	br := bRepo.NewBookRepo()
	t := time.Duration(5 * time.Second)
	bu := bUsecase.NewBookUsecase(br, t)
	httpDelivery.NewHandler(gin, bu)

	gin.Run(":8080")
}
