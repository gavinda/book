package repository

import (
	"bookapp/domain"
)

type bookRepo struct{}

// NewBookRepo ...
func NewBookRepo() domain.BookRepo {
	return &bookRepo{}
}
