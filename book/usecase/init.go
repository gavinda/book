package usecase

import (
	"bookapp/domain"
	"time"
)

type bookUsecase struct {
	bookRepo       domain.BookRepo
	contextTimeout time.Duration
}

// NewBookUsecase ...
func NewBookUsecase(br domain.BookRepo, timeout time.Duration) domain.BookUsecase {
	return &bookUsecase{
		bookRepo:       br,
		contextTimeout: timeout,
	}
}
