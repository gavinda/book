package http

import (
	"bookapp/domain"
	"strconv"

	"github.com/gin-gonic/gin"
)

type BookHandler struct {
	BookUsecase domain.BookUsecase
}

func NewHandler(g *gin.Engine, bu domain.BookUsecase) {
	var handler BookHandler
	handler.BookUsecase = bu
	g.GET("/book", handler.GetBuku)
}

func (b BookHandler) GetBuku(c *gin.Context) {
	ctx := c.Request.Context()
	IDbuku := c.Query("id")
	IDBukuInt, _ := strconv.Atoi(IDbuku)
	buku := b.BookUsecase.AmbilBuku(ctx, IDBukuInt)
	c.JSON(200, buku)
}
